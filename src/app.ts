import  express from 'express';
import cors from  'cors';
import routes from './routes';


const app = express();
app.use(routes);
app.use(cors);



const port = 3001;
app.listen(port, () => {
    console.log(`server running on ${port}`);
});
