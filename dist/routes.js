"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const routes = express_1.Router();
routes.get('/cep', (req, res) => {
    const cep = require('cep-promise');
    cep('42802689').then(console.log);
    res.send(`${cep}`);
});
exports.default = routes;
